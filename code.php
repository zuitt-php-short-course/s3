<?php

class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
        $this->fullName = $firstName . " " . $middleName . " " . $lastName;
        }
    
    public function printName(){
        return "Your full name is $this->fullName.";
        }
}

class Developer extends Person {

    public function printName(){
        return "Your name is $this->fullName and you are a developer.";
    }
}

class Engineer extends Person {

    public function printName(){
        return "You are an engineer named $this->fullName.";
    }
}

$person = new Person('Senku', '', 'Ishigami');
$developer = new Developer('John', 'Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reese');

